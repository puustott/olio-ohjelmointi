/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

/**
 *
 * @author otsop
 */
public class Bottle {
	String name;
	String manufacturer;
	double size;
	double price;
	int type;
	
	public Bottle(String n, double s) { // Lisätään eri tyyppisiä pulloja. Valinta tapahtuu numeroilla 1-5.

		if (n.equals("Pepsi Max")) {
			name = "Pepsi Max";
			manufacturer = "Pepsi";
                        if (s == 0.5) {
                            size = 0.5;
                            price = 1.80;
                            //System.out.println("Pieni pepsi luotu");
                        } if (s == 1.5) {
                            size = 1.5;
                            price = 2.20;
                            //System.out.println("Iso pepsi luotu");
                        }
		} if (n.equals("Coca-Cola Zero")) {
			name = "Coca-Cola Zero";
			manufacturer = "Coca-Cola";
                        if (s == 0.5) {
                            size = 0.5;
                            price = 2.00;
                            //System.out.println("Pieni kokis luotu");
                        } if (s == 1.5) {
                            size = 1.5;
                            price = 2.50;
                            //System.out.println("Iso kokis luotu");
                        }			
		} if (n.equals("Fanta Zero")) {
			name = "Fanta Zero";
			manufacturer = "Fanta";
			size = 0.5;
			price = 1.95;
                        //System.out.println("Fanta luotu");
		} else {
                    //System.out.println("Virhe!");
                }		
	}
	
	public double getPrice() {
		return price;
		// Luodaan lukijametodi hinnalle, koska hinta on privaatti, jotta sitä ei noinvain pääse muuttamaan.
		
	}

}
