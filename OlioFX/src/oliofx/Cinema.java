/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author otsop
 */
public class Cinema {
    String content = "";    
    String line;
    private Document doc;
    private HashMap<String, String> idMap;
    private ArrayList<String> ids = new ArrayList();
    static private Cinema ci = null;

    public HashMap<String, String> getIdMap() {
        return idMap;
    }
    
    private Cinema(){
        URL url = null;
        try {
            url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        } catch (MalformedURLException ex) {
            Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(url.openStream()));
        } catch (IOException ex) {
            Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            while((line = br.readLine()) != null) {
                content = content + line + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            doc = db.parse(new InputSource(new StringReader(content)));            
            doc.getDocumentElement().normalize();
            
            idMap = new HashMap();
            parseData();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    static public Cinema getInstance() {
        if (ci == null)
            ci = new Cinema();
        return ci;
    }
 
    private void parseData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");

        for(int i = 2; i < nodes.getLength(); i ++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
                
            idMap.put(getContent("ID", e), getContent("Name", e));
            ids.add(getContent("ID",e));
            }            
    }
        
    private String getContent(String t, Element e) {
        return ((Element)e.getElementsByTagName(t).item(0)).getTextContent();
    }
        
    public ArrayList<String> getIds() {
        return ids;
    }   
}


