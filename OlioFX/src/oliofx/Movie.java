/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author otsop
 */
public class Movie {
    String content = "";
    String line;
    private Document doc;
    Cinema ca = Cinema.getInstance();
    private ArrayList<String> ids = new ArrayList();
    private HashMap<String, String> idMap;
    private ArrayList<String> movies;
    private ArrayList<String> cinemasAndTimes;
    private String id;
    
    DateTimeFormatter form = DateTimeFormatter.ofPattern("HH:mm");
    DateTimeFormatter form2 = DateTimeFormatter.ISO_LOCAL_DATE_TIME;    
        
    public Movie() {
        ids = ca.getIds();
        idMap = ca.getIdMap();
    }
    
    public void searchByName(String nameMovie, String d, String st, String et) throws IOException, ParserConfigurationException, SAXException {
        URL url = null;
        BufferedReader br = null;
        cinemasAndTimes = new ArrayList();
             for(String id : ids){
                 url = new URL("http://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+d);                
                 br = new BufferedReader(new InputStreamReader(url.openStream()));
                 while((line = br.readLine()) != null) {
                    content = content + line + "\n";
                }
                 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                 DocumentBuilder db = dbf.newDocumentBuilder();
             
                 doc = db.parse(new InputSource(new StringReader(content)));
                 doc.getDocumentElement().normalize();
                
                 parseDataCinemas(st, et, nameMovie);
                 content = "";   
             }

    }
   
    public void listMovies(String nameCinema, String d, String st, String et){
        for(String i : ids){
            if(idMap.get(i).equals(nameCinema)) {
            id = i;                
             }
         }
         URL url = null;
         try {
             url = new URL("http://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+d);
         } catch (MalformedURLException ex) {
             Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
         }

         BufferedReader br = null;
         try {
             br = new BufferedReader(new InputStreamReader(url.openStream()));
         } catch (IOException ex) {
             Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
         }

         try {
             while((line = br.readLine()) != null) {
                 content = content + line + "\n";
             }
         } catch (IOException ex) {
             Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
         }
            
         try {
             DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
             DocumentBuilder db = dbf.newDocumentBuilder();

             doc = db.parse(new InputSource(new StringReader(content)));
             doc.getDocumentElement().normalize();
                 
             movies = new ArrayList();                 
             parseDataMovies(st, et);
             content = "";
                 
         } catch (ParserConfigurationException | SAXException | IOException ex) {
             Logger.getLogger(Cinema.class.getName()).log(Level.SEVERE, null, ex);
         }    
    }
        
    private void parseDataMovies(String st, String et) {
        NodeList nodes = doc.getElementsByTagName("Show");

        for(int i = 2; i < nodes.getLength(); i ++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
               
            LocalTime time = LocalTime.parse(getContent("dttmShowStart", e), form2);
            LocalTime startTime = LocalTime.parse(st, form);
            LocalTime endTime = LocalTime.parse(et, form);
            if(time.isAfter(startTime)) {
                if(time.isBefore(endTime)) {
                movies.add(getContent("Title", e) + " " + time);
                }
            }
        }            
    }    
    private void parseDataCinemas(String st, String et, String nameMovie) {
        
        NodeList nodes = doc.getElementsByTagName("Show");
            
        LocalTime startTime = LocalTime.parse(st, form);
        LocalTime endTime = LocalTime.parse(et, form);
          
        for(int i = 2; i < nodes.getLength(); i ++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            LocalTime time = LocalTime.parse(getContent("dttmShowStart", e), form2);
            
            if(time.isAfter(startTime)) {
                if(time.isBefore(endTime)) {
                    if(nameMovie.equals(getContent("Title",e))){    
                        cinemasAndTimes.add(getContent("Theatre", e) + " " + time);  
                    }
                }
            }
        }            
    }
        
    private String getContent(String t, Element e) {
        return ((Element)e.getElementsByTagName(t).item(0)).getTextContent();
    }
        
    public ArrayList<String> getMovies() {
        return movies;
    }
    public ArrayList<String> getCinemasAndTimes() {
        return cinemasAndTimes;
    }
}
