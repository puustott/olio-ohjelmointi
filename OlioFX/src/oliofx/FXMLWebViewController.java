/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author otsop
 */
public class FXMLWebViewController implements Initializable {
    
    String url;    
    Page pg = new Page();

    @FXML
    private Button refreshButton;
    @FXML
    private Button previousButton;
    @FXML
    private Button nextButton;
    @FXML
    private TextField urlInput;
    @FXML
    private WebView webView;
    @FXML
    private Button initializeButton;
    @FXML
    private Button shoutOutButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load("https://www.google.fi/");
        //pg.addPages("https://www.google.fi");
        
        // TODO
    }    

    @FXML
    private void refreshPage(ActionEvent event) {
        webView.getEngine().reload();
    }

    @FXML
    private void goPreviousPage(ActionEvent event) {
        webView.getEngine().load(pg.getPreviousPage());
        //pg.addPages(webView.getEngine().getLocation());
    }

    @FXML
    private void goNextPage(ActionEvent event) {
        webView.getEngine().load(pg.getNextPage());
    }

    @FXML
    private void goToPage(ActionEvent event) {        
        url = urlInput.getText();
        pg.addPages("https://"+url);
        if(url.equals("index.html")) {
            webView.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        } else {
            webView.getEngine().load("https://" + url);        
        }
    }

    @FXML
    private void initializeAction(ActionEvent event) {
        webView.getEngine().executeScript("initialize()");
    }

    @FXML
    private void shoutOutAction(ActionEvent event) {
        //webView.getEngine()
        webView.getEngine().executeScript("document.shoutOut()");
    }  

    @FXML
    private void findUrl(MouseEvent event) {
        //System.out.println("Klikattu");
        if(webView.getEngine().getLocation().contains(":")){
                pg.addPages(webView.getEngine().getLocation());
            } else {
                pg.addPages("https://"+webView.getEngine().getLocation());
            }
        
    }
}
