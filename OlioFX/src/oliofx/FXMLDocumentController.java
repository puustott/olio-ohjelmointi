/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author otsop
 */
public class FXMLDocumentController implements Initializable {
    String cinema;
    String date;
    String startTime;
    String endingTime;
    String movieName;
    Cinema ca = Cinema.getInstance();
    Movie mo = new Movie();
    
    ArrayList<String> ids = new ArrayList();
    ArrayList<String> movies = new ArrayList();
    ArrayList<String> cinemas = new ArrayList();
    
    DateTimeFormatter form = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    

    @FXML
    private ComboBox<String> cinemaCombo;
    @FXML
    private TextField playDate;
    @FXML
    private TextField beginTime;
    @FXML
    private TextField endTime;
    @FXML
    private Label movieNameLabel;
    @FXML
    private TextField movieNameInput;
    @FXML
    private Button nameSearchButton;
    @FXML
    private ListView<String> movieNameOutput;
    @FXML
    private Button listMovieButton;
    @FXML
    private Label titleLabel;
    @FXML
    private Label cinemaLabel;
    @FXML
    private Label timeLabel;
    @FXML
    private Button webViewButton;
    
    
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ids = ca.getIds();        
        for(String id : ids)
            cinemaCombo.getItems().add(ca.getIdMap().get(id));  
        }      
                
    @FXML
    private void searchByName(ActionEvent event) throws IOException, ParserConfigurationException, SAXException { 
        movieNameOutput.getItems().clear();
        movieName = movieNameInput.getText();
        date = playDate.getText();
        if(date.isEmpty()) {
            LocalDate localDate = LocalDate.now();
            date = form.format(localDate);
        }
        startTime = beginTime.getText();
        endingTime = endTime.getText();
        if(startTime.isEmpty())
            startTime = "00:00";
        if(endingTime.isEmpty())
            endingTime = "23:59";
        mo.searchByName(movieName, date, startTime, endingTime);
        cinemas = mo.getCinemasAndTimes();
        for(String cinema : cinemas) {
            movieNameOutput.getItems().add(cinema);
        } 
    }

    @FXML
    private void listMovies(ActionEvent event) {        
        movieNameOutput.getItems().clear();
        cinema = cinemaCombo.valueProperty().getValue();
        date = playDate.getText();
        if(date.isEmpty()) {
            LocalDate localDate = LocalDate.now();
            date = form.format(localDate);
        }
        startTime = beginTime.getText();
        endingTime = endTime.getText();
        if(startTime.isEmpty())
            startTime = "00:00";
        if(endingTime.isEmpty())
            endingTime = "23:59";
        mo.listMovies(cinema, date, startTime, endingTime);
        movies = mo.getMovies();
        //System.out.println(ids2.toString());
        for(String movie : movies){
            //System.out.println(id2);
            movieNameOutput.getItems().add(movie);
        }
    }

    @FXML
    private void startWeb(ActionEvent event) {
        
        Stage webview = new Stage();
        try {
            Parent page = FXMLLoader.load(getClass().getResource("FXMLWebView.fxml"));
            
            Scene scene = new Scene(page);
            webview.setScene(scene);
            webview.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}