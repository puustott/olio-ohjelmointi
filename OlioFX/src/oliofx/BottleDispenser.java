/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

/**
 *
 * @author otsop
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BottleDispenser {
    
    private int bottles;
    private double money;
    private String amount;
    private Bottle temp;
    private int choice;
    static private BottleDispenser bd = null;
    
    private ArrayList<Bottle> bottleList = new ArrayList();
    
    private BottleDispenser() {
        //bottles = 6;
        money = 0;
        
        bottleList.add(new Bottle("Pepsi Max", 0.5)); // Lisätään viopen määrittämät pullot
        bottleList.add(new Bottle("Pepsi Max", 1.5));
        bottleList.add(new Bottle("Coca-Cola Zero", 0.5));
        bottleList.add(new Bottle("Coca-Cola Zero", 1.5));
        bottleList.add(new Bottle("Fanta Zero", 0.5));
        bottleList.add(new Bottle("Fanta Zero", 0.5));
        
    }
    
    static public BottleDispenser getInstance() {
        if (bd == null)
            bd = new BottleDispenser();
        return bd;
    }
    
    public void addMoney(double m) {
        money = money + m;
    }
    
    public String buyBottle(int i) {
    	choice = i; // Uusi muuttuja käyttöön, jolla mahdollistetaan pullon valinta
        bottles = bottleList.size();
        if (bottles < 1) { //Estetään pullojen määrän negatiivisuus
            return ("Ei pulloja!");
        }
    	temp = bottleList.get(choice - 1);
    	if (money < temp.getPrice()) { //Estetään rahatta ostaminen. Vertailu nyt get.Price() -metodin avulla.
    		return ("Syötä rahaa ensin!");
    	} else {
            money -= bottleList.get(choice - 1).getPrice(); // Vähennetään pullon hinta jo koneessa olevasta rahamäärästä
            bottles -= 1;
            bottleList.remove(choice - 1);
            return ("KACHUNK! " + temp.name + " tipahti masiinasta!");
            // Sana "pullo" vaihdettiin listassa ylimpänä olevan pullon nimeen
            
        }
    }
    public String buyBottleFX(String n, double s) throws IOException {
      
        bottles = bottleList.size();
        if (bottles < 1) { //Estetään pullojen määrän negatiivisuus
            return "Ei pulloja!";
        }  
        for (Bottle bottle : bottleList) {
            if (bottle.name.equals(n)) {
                 if (bottle.size == s) {
                  
                     if (money >= bottle.getPrice()) {
                        money -= bottle.getPrice();
                        BufferedWriter out = new BufferedWriter(new FileWriter("Receipt.txt"));
                        out.write("*** Kuitti ***");
                        out.newLine();
                        out.write("Viimeisin ostoksesi: ");
                        out.newLine();
                        out.write(bottle.name + " " + bottle.size +"L "+ bottle.price +"€.");
                        out.close();
                        bottleList.remove(bottle);
                        return ("KACHUNK! " + bottle.name + " tipahti masiinasta!");                        
                    } else {
                        return "Syötä rahaa ensin!";  
                    }
                 }
            }   
        }
        return "Pulloa ei ole!";
    }
    
    /*public void returnMoney() {
    	amount = String.format("%.2f", money).replace(".", ","); // Muutetaan rahamäärä doublesta stringiksi, 2 desimaalia ja pisteet pilkuiksi.
    	System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + amount +"€");
        money = 0;        
    } */
    
    public ArrayList getBottles() {    	// Palautetaan ArrayList pääohjelmalle, jotta listan koon saa helpommin selville
    	return bottleList;
    }
    
    public Bottle getBottles(int i) {    // Tämän avulla saadaan selville pullon tiedot	
    	return bottleList.get(i);
    }
    public String returnMoney() {
        amount = String.format("%.2f", money).replace(".", ",");
        money = 0;
        return amount;
    }
    public double getMoney() {
        return money;
    }
}
