/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author otsop
 */
public class Page {
    private ArrayList<String> previousPages = new ArrayList<>();
    private ArrayList<String> nextPages = new ArrayList<>();
    ListIterator preLitr = previousPages.listIterator();
    ListIterator nexLitr = nextPages.listIterator();
    private String page;
    private int tempPrev = 1;
    private int tempNext = 0;
    
    public Page() {
    previousPages.add("https://www.google.fi");
    }
    
    public void addPages(String p) {        
        previousPages.add(p); 
        if(previousPages.size()>10) {
            previousPages.remove(0);
            //System.out.println("Poistettu");
        }
        //System.out.println(previousPages.toString());
    }
    
    public String getPreviousPage() {
        nextPages.add(previousPages.get(previousPages.size() - tempPrev));
        ListIterator preLitr = previousPages.listIterator(previousPages.size() - tempPrev);
        if(preLitr.hasPrevious()){
            page = (String) preLitr.previous();
            
            //System.out.println(nextPages.toString());
            tempPrev = tempPrev + 1;
            return page;
       } else {
           return "https://www.lut.fi";
       }
    }
    public String getNextPage() {
        nexLitr = nextPages.listIterator(nextPages.size() - tempNext);
        if(nexLitr.hasPrevious()){
            page = (String) nexLitr.previous();
            tempNext = tempNext + 1;
            return page;
        } else {
            return "https://www.lut.fi";
        }
    } 
}
   
    

