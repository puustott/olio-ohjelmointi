/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

/**
 *
 * @author otsop
 */
import java.util.Scanner;

public class BottleTUI {
	
	public static void main(String[] args) {
		int choice;
		int choiceBottle;
		
		BottleDispenser bd = BottleDispenser.getInstance(); 
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println();
			System.out.println("*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lisää rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			choice = sc.nextInt();
			if (choice == 0) {
				break;
			} else if (choice == 1) {
				bd.addMoney(1.0);
			} else if (choice == 2) {
				for (int i = 0; i<bd.getBottles().size();i++) {
					String name = bd.getBottles(i).name;
					Double size = bd.getBottles(i).size;
					Double price = bd.getBottles(i).getPrice();
					// Otetaan pullojen tiedot ylös yksi kerrallaan ja printataan näytölle
					System.out.println(i+1 + ". Nimi: " + name);
					System.out.println("\t" + "Koko: " + size + "\t" + "Hinta: " + price);
				}
				System.out.print("Valintasi: ");
				choiceBottle = sc.nextInt(); // Tallennetaan käyttäjän antama arvo muuttujaan
				bd.buyBottle(choiceBottle); // ja lähetetaan se buyBottlelle.
			} else if (choice == 3) {
				bd.returnMoney();
			} if (choice == 4) {
				for (int i = 0; i<bd.getBottles().size();i++) {
					String name = bd.getBottles(i).name;
					Double size = bd.getBottles(i).size;
					Double price = bd.getBottles(i).getPrice();
					// Otetaan pullojen tiedot ylös yksi kerrallaan ja printataan näytölle
					System.out.println(i+1 + ". Nimi: " + name);
					System.out.println("\t" + "Koko: " + size + "\t" + "Hinta: " + price);
				}
				
			}
		}

	}

}
