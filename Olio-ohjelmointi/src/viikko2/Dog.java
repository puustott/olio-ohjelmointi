package viikko2;

import java.util.Scanner;

public class Dog {
	private String name;
	private Scanner saying;
	private String temp;
	
	public Dog(String n) {
		name = n;
		if (name.trim().isEmpty()) { // Karsitaan v�lily�nnit ja testataan onko k�ytt�j� antanut tyhj�n.
			name = "Doge";
			System.out.println("Hei, nimeni on " + name);
		} else {
		System.out.println("Hei, nimeni on " + name);
		}
	}
	
	public void speak(String s) {
		temp = s;
		saying = new Scanner(temp); // Muutos v�liaikaismuuttujalta takaisin Scanner-muotoon. Java ei tyk�nnyt suoraan Scanner muodosta, koska tekstikentt� j�i avoimeksi
		while (saying.hasNext()) { // V�liaikaismuuttujan avulla looppi ei j�� p��lle loputtomasti.
			if (saying.hasNextBoolean()) {
				System.out.println("Such boolean: " + saying.nextBoolean());
				
			} if (saying.hasNextInt()) {
				System.out.println("Such integer: " + saying.nextInt());
				
			} else if (saying.hasNext()) { // Jos seuraava sana ei ole integer tai boolean, niin se varmaan on string.
				System.out.println(saying.next());
							
			} else {
				break;
			}
		}
		
		
	}

}

