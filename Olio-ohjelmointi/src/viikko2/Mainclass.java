package viikko2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Mainclass {
	
	public static void main(String[] args) throws IOException {
		String name;
		String saying;
		Scanner temp;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Anna koiralle nimi: ");
		name = br.readLine();
		
		Dog d1 = new Dog(name);
		
		System.out.print("Mitä koira sanoo: ");
		temp = new Scanner(System.in); // Otetaan käyttäjän antama teksti väliaikaismuuttujalle scanneri-muodossa
		saying = temp.nextLine(); // Tallennetaan käyttäjän antama teksti stringiksi
		temp.close();
		
		d1.speak(saying);
	}
}
