package viikko6;

import java.util.Scanner;

public class Mainclass {
	
	public static void main(String[] args) {
		
		int choice;
		String accNumber;
		int moneyAmount;
		String accNumberCredit;
		int moneyAmountCredit;
		int creditLimit;
		
		Scanner sc = new Scanner(System.in);
		Bank ba = new Bank();
	
		while (true) {
			System.out.println();
			System.out.println("*** PANKKIJ�RJESTELM� ***");
			System.out.println("1) Lis�� tavallinen tili");
			System.out.println("2) Lis�� luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa");
			System.out.println("4) Nosta tililt�");
			System.out.println("5) Poista tili");
			System.out.println("6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			choice = sc.nextInt();
			if (choice == 0) {
				break;
			} if (choice == 1) {
				System.out.print("Sy�t� tilinumero: ");		
				accNumber = sc.next();
				System.out.print("Sy�t� raham��r�: ");
				moneyAmount = sc.nextInt();
				ba.addBankAccount(accNumber, moneyAmount);
				continue;
			} if (choice == 2) {
				System.out.print("Sy�t� tilinumero: ");		
				accNumberCredit = sc.next();
				System.out.print("Sy�t� raham��r�: ");
				moneyAmountCredit = sc.nextInt();
				System.out.print("Sy�t� luottoraja: ");
				creditLimit = sc.nextInt();
				ba.addCreditAccount(accNumberCredit, moneyAmountCredit, creditLimit);
				continue;
			} if (choice == 3) {
				System.out.print("Sy�t� tilinumero: ");		
				accNumber = sc.next();
				System.out.print("Sy�t� raham��r�: ");
				moneyAmount = sc.nextInt();
				ba.depositMoney(accNumber, moneyAmount);
				continue;
			} if (choice == 4) {
				System.out.print("Sy�t� tilinumero: ");		
				accNumber = sc.next();
				System.out.print("Sy�t� raham��r�: ");
				moneyAmount = sc.nextInt();
				ba.withdrawMoney(accNumber, moneyAmount);
				continue;
			} if (choice == 5) {
				System.out.print("Sy�t� poistettava tilinumero: ");		
				accNumber = sc.next();
				ba.removeAccount(accNumber);
				continue;
			} if (choice == 6) {
				System.out.print("Sy�t� tulostettava tilinumero: ");		
				accNumber = sc.next();
				ba.find(accNumber);
				continue;
			} if (choice == 7) {
				ba.find();	
				continue;
			} else {
				System.out.println("Valinta ei kelpaa.");
			}
		}
		sc.close();
	}
}
