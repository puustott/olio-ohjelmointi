package viikko6;

import java.util.ArrayList;

public class Bank {
	private String accountNumber;
	private int money;
	private int credit;
	ArrayList<Account> accounts = new ArrayList();
	ArrayList<Account> creditAccounts = new ArrayList();
	
	
	public Bank() {

	}
	
	public void addBankAccount(String n, int m) {
		accountNumber = n;
		money = m;
		BankAccount ba = new BankAccount(n,m);
		accounts.add(ba);
		System.out.println("Tili luotu.");
		
	}
	
	public void addCreditAccount(String n, int m, int c) {
		accountNumber = n;
		money = m;
		credit = c;
		CreditAccount ca = new CreditAccount(n, m, c);
		creditAccounts.add(ca);
		System.out.println("Tili luotu.");
	}
	
	public void depositMoney(String n, int m) {
		accountNumber = n;
		money = m;
		for(Account account : accounts) {
			if (account.accNumber.equals(accountNumber)) {
				account.money = account.money + money;
			}
		}
		for(Account creditAccount : creditAccounts) {
			if (creditAccount.accNumber.equals(accountNumber)) {
				creditAccount.money = creditAccount.money + money;
			}
		}
	}
	
	public void withdrawMoney(String n, int m) {
		accountNumber = n;
		money = m;
		for(Account account : accounts) {
			if (account.accNumber.equals(accountNumber)) {
				account.money = account.money - money;
				if (account.money < 0) {
					System.out.println("Tilin kateraja ylitetty!");
				}
			}
		}
		for(Account creditAccount : creditAccounts) {
			if (creditAccount.accNumber.equals(accountNumber)) {
				creditAccount.money = creditAccount.money - money;
			} if (creditAccount.money < -credit) {
				System.out.println("Tilin kateraja ylitetty!");
			}
		}
	}
	
	public void removeAccount(String n) {
		accountNumber = n;	
		for(Account account : accounts) {
			if (account.accNumber.equals(accountNumber)) {
				accounts.remove(account);
				System.out.println("Tili poistettu.");
				break;
			}
		}
	}
	
	public void find(String n) {
		accountNumber = n;
		for(Account account : accounts) {
			if (account.accNumber.equals(accountNumber)) {
				System.out.println("Tilinumero: " + account.accNumber + " Tilill� rahaa: " + account.money);
			}
		}
		for(Account creditAccount : creditAccounts) {
			if (creditAccount.accNumber.equals(accountNumber)) {
				System.out.println("Tilinumero: " + creditAccount.accNumber + " Tilill� rahaa: " + creditAccount.money);
			}
		}
	}
	
	public void find() {
		System.out.println("Kaikki tilit:");
		for(Account account : accounts) {
				System.out.println("Tilinumero: " + account.accNumber + " Tilill� rahaa: " + account.money);
		}
		for(Account creditAccount : creditAccounts) {
			if (creditAccount.accNumber.equals(accountNumber)) {
				System.out.println("Tilinumero: " + creditAccount.accNumber + " Tilill� rahaa: " + creditAccount.money + " Luottoraja: " + creditAccount.credit);
			}
		}
	}
}


	


