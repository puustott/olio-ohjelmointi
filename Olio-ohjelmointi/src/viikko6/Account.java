package viikko6;

abstract class Account {
	protected String accNumber;
	protected int money;
	protected int credit;
	
	public Account(String n, int m) {
		accNumber = n;
		money = m;		
	}
}

class BankAccount extends Account {
	public BankAccount(String n, int m) {
		super(n, m);
		
	}	
}

class CreditAccount extends Account {
	
	public CreditAccount(String n, int m, int c) {
		super(n, m);
		credit = c;
	}	
}
	
