package viikko5;

import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {
		
		int choice;
		int choiceChar;
		int choiceWeap;
		String tempChar = null;
		String tempWeap = null;
				
		Scanner sc = new Scanner(System.in);
		
		while (true) {
			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			choice = sc.nextInt();
			if (choice == 0) {
				break;
			} if (choice == 1) {
				System.out.println("Valitse hahmosi: ");
				System.out.println("1) Kuningas");
				System.out.println("2) Ritari");
				System.out.println("3) Kuningatar");
				System.out.println("4) Peikko");
				System.out.print("Valintasi: ");
				choiceChar = sc.nextInt();
				System.out.println("Valitse aseesi: ");
				System.out.println("1) Veitsi");
				System.out.println("2) Kirves");
				System.out.println("3) Miekka");
				System.out.println("4) Nuija");
				System.out.print("Valintasi: ");
				choiceWeap = sc.nextInt();
				Character character = new Character(choiceChar, choiceWeap);
				tempChar = character.nameCharacter;
				tempWeap = character.nameWeapon;
			} if (choice == 2) {
				System.out.println(tempChar + " tappelee aseella " + tempWeap);				
			} 			
		}		
		sc.close();
	}
}