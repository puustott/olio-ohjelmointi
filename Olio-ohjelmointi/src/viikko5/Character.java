package viikko5;

public class Character {
	int choiceChar;
	int choiceWea;
	int choice;
	public String nameCharacter;
	public String nameWeapon;
	String name;
	
	private class King {
		
		private String weaponSelect;
		private String name;
		
		private King(int i) {
			name = "King";
			choiceWea = i;
			Weapon weapon = new Weapon(choiceWea);
			weaponSelect = weapon.nameWeapon;
			
		}
	}
	
	private class Knight {
		
		private String weaponSelect;
		private String name;
		
		private Knight(int i) {
			name = "Knight";
			choiceWea = i;
			Weapon weapon = new Weapon(choiceWea);
			weaponSelect = weapon.nameWeapon;
			
		}
	}
	
	private class Queen {
		
		private String weaponSelect;
		private String name;
		
		private Queen(int i) {
			name = "Queen";
			choiceWea = i;
			Weapon weapon = new Weapon(choiceWea);
			weaponSelect = weapon.nameWeapon;
			
		}
	}
	
	private class Troll {
		
		private String weaponSelect;
		private String name;
		
		private Troll(int i) {
			name = "Troll";
			choiceWea = i;
			Weapon weapon = new Weapon(choiceWea);
			weaponSelect = weapon.nameWeapon;
			
		}
	}
	
	public class Weapon {
		
		private String nameWeapon;
		
		private Weapon(int i) {
			choiceWea = i;
			if (choiceWea == 1) {
				Knife knife = new Knife();
				nameWeapon = knife.name;
			} if (choiceWea == 2) {
				Axe axe = new Axe();
				nameWeapon = axe.name;
			} if (choiceWea == 3) {
				Sword sword = new Sword();
				nameWeapon = sword.name;
			} if (choiceWea == 4) {
				Club club = new Club();
				nameWeapon = club.name;
			}
		}
	
		private class Knife {
			private String name;

			private Knife() {
				name = "Knife";
			}
		}
		
		private class Axe {
			private String name;

			private Axe() {
				name = "Axe";
			}
		}
		
		private class Sword {
			private String name;

			private Sword() {
				name = "Sword";
			}
		}
		
		private class Club {			
			private String name;

			private Club() {
				name = "Club";
			}
		}
	}
	
	public Character(int i,int j) {
		choiceChar = i;
		choiceWea = j;
		
		if (choiceChar == 1) {
			King king = new King(choiceWea);
			nameCharacter = king.name;
			nameWeapon = king.weaponSelect;
		} if (choiceChar == 2) {
			Knight knight = new Knight(choiceWea);
			nameCharacter = knight.name;
			nameWeapon = knight.weaponSelect;
		} if (choiceChar == 3) {
			Queen queen = new Queen(choiceWea);
			nameCharacter = queen.name;
			nameWeapon = queen.weaponSelect;
		} if (choiceChar == 4) {
			Troll troll = new Troll(choiceWea);
			nameCharacter = troll.name;
			nameWeapon = troll.weaponSelect;
		}
	}
}
