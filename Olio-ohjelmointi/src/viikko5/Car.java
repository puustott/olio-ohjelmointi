package viikko5;

public class Car {
	
	public class Body {
		
		public Body() {
			
			System.out.println("Valmistetaan: Body");
		}

	}
	
	public class Chassis {
		
		public Chassis() {
			
			System.out.println("Valmistetaan: Chassis");
		}
	}
	
	public class Engine {
		
		public Engine() {
			
			System.out.println("Valmistetaan: Engine");
		}		
	}
	
	public class Wheel {
		
		public Wheel () {
			
			System.out.println("Valmistetaan: Wheel");
		}
	}

	public Car() {
		Body body = new Body();
		Chassis chassis = new Chassis();
		Engine engine = new Engine();
		Wheel wheel1 = new Wheel();
		Wheel wheel2 = new Wheel();
		Wheel wheel3 = new Wheel();
		Wheel wheel4 = new Wheel();
	}
	
	public void print() {
		System.out.println("Autoon kuuluu:");
		System.out.println("\t Body");
		System.out.println("\t Chassis");
		System.out.println("\t Engine");
		System.out.println("\t 4 Wheel");
		
	}
}

