package viikko3;

public class Bottle {
	String name;
	String manufacturer;
	double size;
	private double price;
	int type;
	
	public Bottle(int i) { // Lis�t��n eri tyyppisi� pulloja. Valinta tapahtuu numeroilla 1-5.
		type = i;
		if (type == 1) {
			name = "Pepsi Max";
			manufacturer = "Pepsi";
			size = 0.5;
			price = 1.80;
		} if (type == 2) {
			name = "Pepsi Max";
			manufacturer = "Pepsi";
			size = 1.5;
			price = 2.20;
		} if (type == 3) {
			name = "Coca-Cola Zero";
			manufacturer = "Coca-Cola";
			size = 0.5;
			price = 2.00;
		} if (type == 4) {
			name = "Coca-Cola Zero";
			manufacturer = "Coca-Cola";
			size = 1.5;
			price = 2.50;
		} if (type == 5) {
			name = "Fanta Zero";
			manufacturer = "Fanta";
			size = 0.5;
			price = 1.95;
		}		
	}
	
	public double getPrice() {
		return price;
		// Luodaan lukijametodi hinnalle, koska hinta on privaatti, jotta sit� ei noinvain p��se muuttamaan.
		
	}

}
