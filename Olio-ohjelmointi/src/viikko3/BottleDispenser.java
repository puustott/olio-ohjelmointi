package viikko3;

import java.util.ArrayList;

public class BottleDispenser {
    
    private int bottles;
    private double money;
    private String amount;
    private Bottle temp;
    private int choice;
    
    private ArrayList<Bottle> bottleList = new ArrayList();
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        bottleList.add(new Bottle(1)); // Lis�t��n viopen m��ritt�m�t pullot
        bottleList.add(new Bottle(2));
        bottleList.add(new Bottle(3));
        bottleList.add(new Bottle(4));
        bottleList.add(new Bottle(5));
        bottleList.add(new Bottle(5));
        
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lis�� rahaa laitteeseen!");
    }
    
    public void buyBottle(int i) {
    	choice = i; // Uusi muuttuja k�ytt��n, jolla mahdollistetaan pullon valinta
    	temp = bottleList.get(choice - 1);
    	if (money < temp.getPrice()) { //Estet��n rahatta ostaminen. Vertailu nyt get.Price() -metodin avulla.
    		System.out.println("Sy�t� rahaa ensin!");
    	} else if (bottles < 0) { //Estet��n pullojen m��r�n negatiivisuus
    		System.out.println("Ei pulloja!");
    	} else {
    		money -= bottleList.get(choice - 1).getPrice(); // V�hennet��n pullon hinta jo koneessa olevasta raham��r�st�
        	System.out.println("KACHUNK! " + temp.name + " tipahti masiinasta!");
        	// Sana "pullo" vaihdettiin listassa ylimp�n� olevan pullon nimeen
        	bottles -= 1;
        	bottleList.remove(choice - 1);
    	}
    }
    
    public void returnMoney() {
    	amount = String.format("%.2f", money).replace(".", ","); // Muutetaan raham��r� doublesta stringiksi, 2 desimaalia ja pisteet pilkuiksi.
    	System.out.println("Klink klink. Sinne meniv�t rahat! Rahaa tuli ulos " + amount +"�");
        money = 0;        
    }
    
    public ArrayList getBottles() {    	// Palautetaan ArrayList p��ohjelmalle, jotta listan koon saa helpommin selville
    	return bottleList;
    }
    
    public Bottle getBottles(int i) {    // T�m�n avulla saadaan selville pullon tiedot	
    	return bottleList.get(i);
    }
    
}
