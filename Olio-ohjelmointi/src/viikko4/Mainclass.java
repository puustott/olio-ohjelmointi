package viikko4;

import java.io.IOException;

public class Mainclass {
		
	public static void main(String[] args) throws IOException {
				
		ReadAndWriteIO rawio = new ReadAndWriteIO("input.txt");
		rawio.readAndWriteText("zipinput.zip");		
	}
}
