package viikko4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ReadAndWriteIO {
	private String filename;
	private ZipFile zip;
	
	public ReadAndWriteIO(String s) {
		filename = s;		
	}
	
	public void readFile() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(filename));
		while (in.ready()) {
			System.out.println(in.readLine());
		}
		in.close();
				
	}
	
	public void readAndWriteText(String readname) throws IOException {
		zip = new ZipFile(readname); // M��ritet��n zip-tiedoston nimi
		Enumeration<? extends ZipEntry> entry = zip.entries(); // Etsit��n zipin sis�ll� olevat tiedostot
		ZipEntry zipEntry = entry.nextElement(); // Otetaan talteen ensimm�inen tiedosto zipin sis�ll�
		InputStream in = zip.getInputStream(zipEntry); // Luetaan tekstitiedosto.
		
		BufferedReader br = new BufferedReader(new InputStreamReader(in)); // Muutetaan InputStreamin lukema teksti sellaiseksi, ett� ihminenkin sen ymm�rt��.
		while (br.ready()) {
			System.out.println(br.readLine());
			}
		br.close();
	}

}


